NAME=nos18
all: build

build:
	@echo "\n\n\n"
	@echo Building ${NAME}
	@[ -e ${NAME}.tex ] && pdflatex ${NAME}.tex
clean:
	@ [ -e ${NAME}.aux  ] && rm ${NAME}.aux
	@ [ -e ${NAME}.log  ] && rm ${NAME}.log
	@ [ -e ${NAME}.nav  ] && rm ${NAME}.nav
	@ [ -e ${NAME}.snm  ] && rm ${NAME}.snm
	@ [ -e ${NAME}.out  ] && rm ${NAME}.out
	@ [ -e ${NAME}.toc  ] && rm ${NAME}.toc
	@ [ -e texput.log ] && rm texput.log
